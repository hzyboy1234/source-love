```
🕙 分享是一种美德，右上随手点个 🌟 Star，谢谢 
```

### 前言
曾经，作者也为寻求自己的另一半苦恼，因为平时工作繁忙，交际圈窄小，而父母又各种催婚，无奈上了“XX网”去碰碰运气。
好不容易，在它的相亲资料库里看到自己心仪的对象，于是找红娘前去索要联系方式，被告知至少需要交1280元会员费（半年为期，可自由选择12个会员编号）。
尼玛，虽说现在是知识付费时代，但这也太贵了，对于工薪阶层的我来说，可是一笔不小的开支，而且或许我只需要联系上一个人就情定终身了呢，那我的钱不是白白浪费了嘛！
就没有按次付费的吗？“XX网”答复没有，必须至少先交1280元。
于是，我们团队思前想后，开发运营“寻缘日记”，帮助拥有同样烦恼的你。 

## 演示地址

- H5：<http://demo.sourcebyte.cn:8080/love>
- 后台：<http://boot.sourcebyte.cn:8081/login>  
- 官网：<http://boot.sourcebyte.cn>  

## 部分截图

### 1. 后台截图
![输入图片说明](https://gitee.com/open-source-byte/source-mall/raw/master/doc/5.png)
![输入图片说明](https://gitee.com/open-source-byte/source-mall/raw/master/doc/6.png)

### 2. 移动端截图
<p align="center">
<img alt="logo" src="https://gitee.com/open-source-byte/source-love/raw/master/doc/image/0.jpg" style="width:100px;">
</p>


## 开发文档
这代码有没有文档呀？ 当然有啦

http://doc.sourcebyte.cn:8082/index.html

### 作者信息

1.  作者：詹Sir
2.  邮箱：261648947@qq.com
3.  官网：sourcebyte.cn

### 交流群
<p align="left">
<img alt="logo" src="https://img-blog.csdnimg.cn/df9928e2ebe6497f94fb7fe1a207ced7.jpg" width="200">
</p>